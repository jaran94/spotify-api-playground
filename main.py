import spotipy
from spotipy.oauth2 import SpotifyOAuth, SpotifyClientCredentials
import os
from dotenv import load_dotenv

"""
Create environmental variables in credentials.env file:
CLIENT_ID=your_client_id
CLIENT_SECRET=your_client_secret 
USERNAME=your_username
"""

load_dotenv("credentials.env")

CLIENT_ID = os.environ.get("CLIENT_ID")
CLIENT_SECRET = os.environ.get("CLIENT_SECRET")
USERNAME = os.environ.get("USERNAME")
REDIRECT_URI = "https://example.com"


searcher = spotipy.Spotify(auth_manager=SpotifyClientCredentials(client_id=CLIENT_ID,
                                                                 client_secret=CLIENT_SECRET))

user_playback = spotipy.Spotify(auth_manager=SpotifyOAuth(client_id=CLIENT_ID,
                                                          client_secret=CLIENT_SECRET,
                                                          redirect_uri=REDIRECT_URI,
                                                          scope="user-modify-playback-state,user-read-playback-state",
                                                          cache_path="token_playback.txt",
                                                          username=USERNAME,
                                                          open_browser=True))

def play_the_album():
    artist = input("State the artist:\n").strip().lower()
    album = input("State the album:\n").strip().lower()
    result = searcher.search(f"{artist} {album}", limit=1, type="album")
    album_id = result.get("albums").get("items")[0].get("id")
    try:
        user_playback.start_playback(context_uri=f"spotify:album:{album_id}")
    except spotipy.exceptions.SpotifyException as e:
        print(f"Exception: {e}. \nOpen player on any device to start album.")


def decision_prompt():
    question = input("Do you want to search again? Type Y/N\n").strip().lower()
    if question == "y":
        return True
    elif question == "n":
        return False
    else:
        print("You need to check your sight, type y/n again")
        decision_prompt()


if __name__ == "__main__":
    decision = True
    while decision:
        play_the_album()
        decision = decision_prompt()
